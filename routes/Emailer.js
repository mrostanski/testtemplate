var Emailer, emailTemplates, emailer, exports, path, templates_options;

path = require("path");

emailer = require("nodemailer");

emailTemplates = require("swig-email-templates");

templates_options = {
    root: path.join(__dirname, "../views/")
};

Emailer = (function() {
    var cfg;

    Emailer.prototype.attachments = [
        {
            fileName: "logo.png",
            path: path.join(__dirname, "../public/images/logo.png"),
            cid: "logo@bulletn.incuvo.com"
        }, {
            fileName: "avatarAnony.png",
            path: path.join(__dirname, "../public/images/avatarAnony.png"),
            cid: "avatar_anony@bulletn.incuvo.com"
        }, {
            fileName: "appstore.png",
            path: path.join(__dirname, "../public/images/appstore.png"),
            cid: "appstore@bulletn.incuvo.com"
        }, {
            fileName: "people.png",
            path: path.join(__dirname, "../public/images/people.png"),
            cid: "people@bulletn.incuvo.com"
        }
    ];

    cfg = {};

    function Emailer(cfg1) {
        this.cfg = cfg1;
    }

    Emailer.prototype.send = function(options, callback) {
        var self;
        self = this;
        return emailTemplates(templates_options, (function(options) {
            return function(err, render) {
                if (err) {
                    return callback(err);
                }
                return render(options.template, options.context, (function(options) {
                    return function(err, html, text) {
                        var attachments, messageData, transport;
                        if (err) {
                            return callback(err);
                        } else {
                            attachments = self.getAttachments(html);
                            messageData = {
                                to: "'" + options.to.name + " " + options.to.surname + "' <" + options.to.email + ">",
                                from: options.from.name + " " + options.from.surname + " via Bulletn <notification@bulletn.incuvo.com>",
                                subject: options.subject,
                                html: html,
                                text: text,
                                attachments: attachments
                            };
                            transport = self.getTransport();
                            return transport.sendMail(messageData, callback);
                        }
                    };
                })(options));
            };
        })(options));
    };

    Emailer.prototype.getTransport = function() {
        return emailer.createTransport(this.cfg.transport);
    };

    Emailer.prototype.getAttachments = function(html) {
        var attachment, attachments, i, len, ref;
        attachments = [];
        ref = this.attachments;
        for (i = 0, len = ref.length; i < len; i++) {
            attachment = ref[i];
            if (html.search("cid:" + attachment.cid) > -1) {
                attachments.push(attachment);
            }
        }
        return attachments;
    };

    return Emailer;

})();

exports = module.exports = Emailer;