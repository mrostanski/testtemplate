var notifyUserByEmail,
    indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

notifyUserByEmail = function(req, post) {
    if (post && post.groups.length > 0) {
        return mm.User.find({
            email: {
                '$in': post.groups
            }
        }, function(err, users) {
            var avatar_text, avatar_url, email_options, firstname, fn, found_users, i, j, lastname, len, len1, ref, results, user, user_email, user_email_options;
            if (err) {
                util.log("[Emailer] Post news - Failed to get users\n post:" + post.id + "  err:" + err);
                return;
            }
            avatar_text = "";
            firstname = ustr.titleize(post.owner.firstname);
            lastname = ustr.titleize(post.owner.lastname);
            if (firstname) {
                avatar_text = firstname[0];
            }
            if (lastname) {
                avatar_text = avatar_text + lastname[0];
            }
            avatar_url = 'cid:avatar_anony@bulletn.incuvo.com';
            if (avatar_text) {
                avatar_text = qs.escape(avatar_text);
                avatar_url = "http://api.bulletn.incuvo.com/v1/literalAvatar?client_id=oxGCE1Hypile7yys3sUJmlMOGXjcAswc&client_secret=NQnR2jVQTOTHmmNcK9c6xWmDIc5dqq00&text=" + avatar_text + "&w=64&h=64";
            }
            email_options = {
                to: {
                    email: '',
                    name: '',
                    surname: ''
                },
                from: {
                    name: firstname,
                    surname: lastname
                },
                subject: "Check out this article: \"" + (ustr.truncate(post.web_title, 61)) + "\"",
                template: "post.html",
                context: {
                    img_logo: 'cid:logo@bulletn.incuvo.com',
                    avatar_anony: avatar_url,
                    img_appstore: 'cid:appstore@bulletn.incuvo.com',
                    img_people: 'cid:people@bulletn.incuvo.com',
                    origin: ustr.truncate(post.web_hostname, 64),
                    comment: post.description,
                    description: post.selected_text ? ustr.truncate(post.selected_text, 125) : ustr.truncate(post.web_title, 128),
                    selected_text: post.selected_text,
                    web_hostname: post.web_hostname,
                    web_title: post.web_title,
                    web_url: post.web_url,
                    tags: (post.tags.map(function(val) {
                        return '#' + val;
                    })).slice(0, 5).join(' '),
                    taggedUsers: ustr.truncate((post.taggedUsers || []).join(", "), 128),
                    groups: (users.map(function(val) {
                        return ustr.titleize(val.firstname) + ' ' + ustr.titleize(val.lastname);
                    })).slice(0, 2).join(' '),
                    firstname: ustr.titleize(post.owner.firstname),
                    lastname: ustr.titleize(post.owner.lastname),
                    email: post.owner.email,
                    web_media_url: post.web_media_url,
                    sender_email: post.owner.email || "No email",
                    skip_unsubscribe: false
                }
            };
            found_users = [];
            fn = function(user, user_email_options, req) {
                if (!user.is_unsubscribe) {
                    firstname = ustr.titleize(user.firstname);
                    lastname = ustr.titleize(user.lastname);
                    user_email_options.context.user_token = req.app.bulletn.config.tokensCodeHashes.encodeHex(user._id.toHexString());
                    user_email_options.to.email = user.email;
                    user_email_options.to.name = firstname;
                    user_email_options.to.surname = lastname;
                    return req.app.bulletn.emailer.send(user_email_options, function(err, result) {
                        if (err) {
                            return util.log("[Emailer] Post news - Failed to send post email\n user:" + user.id + "  err:" + err);
                        }
                    });
                }
            };
            for (i = 0, len = users.length; i < len; i++) {
                user = users[i];
                found_users.push(user.email.trim());
                user_email_options = u.clone(email_options);
                fn(user, user_email_options, req);
            }
            ref = post.groups;
            results = [];
            for (j = 0, len1 = ref.length; j < len1; j++) {
                user_email = ref[j];
                user_email = user_email.trim();
                if (indexOf.call(found_users, user_email) < 0) {
                    user_email_options = u.clone(email_options);
                    user_email_options.context.user_token = '';
                    user_email_options.context.skip_unsubscribe = true;
                    user_email_options.to.email = user_email;
                    user_email_options.to.name = '';
                    user_email_options.to.surname = '';
                    results.push((function(user_email_options, req) {
                        return req.app.bulletn.emailer.send(user_email_options, function(err, result) {
                            if (err) {
                                return util.log("[Emailer] Post news - Failed to send post email\n user:" + user.id + "  err:" + err);
                            }
                        });
                    })(user_email_options, req));
                } else {
                    results.push(void 0);
                }
            }
            return results;
        });
    }
};
